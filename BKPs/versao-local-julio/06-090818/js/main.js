// jQuery Mask Plugin v1.14.15
// github.com/igorescobar/jQuery-Mask-Plugin
var $jscomp={scope:{},findInternal:function(a,l,d){a instanceof String&&(a=String(a));for(var p=a.length,h=0;h<p;h++){var b=a[h];if(l.call(d,b,h,a))return{i:h,v:b}}return{i:-1,v:void 0}}};$jscomp.defineProperty="function"==typeof Object.defineProperties?Object.defineProperty:function(a,l,d){if(d.get||d.set)throw new TypeError("ES3 does not support getters and setters.");a!=Array.prototype&&a!=Object.prototype&&(a[l]=d.value)};
$jscomp.getGlobal=function(a){return"undefined"!=typeof window&&window===a?a:"undefined"!=typeof global&&null!=global?global:a};$jscomp.global=$jscomp.getGlobal(this);$jscomp.polyfill=function(a,l,d,p){if(l){d=$jscomp.global;a=a.split(".");for(p=0;p<a.length-1;p++){var h=a[p];h in d||(d[h]={});d=d[h]}a=a[a.length-1];p=d[a];l=l(p);l!=p&&null!=l&&$jscomp.defineProperty(d,a,{configurable:!0,writable:!0,value:l})}};
$jscomp.polyfill("Array.prototype.find",function(a){return a?a:function(a,d){return $jscomp.findInternal(this,a,d).v}},"es6-impl","es3");
(function(a,l,d){"function"===typeof define&&define.amd?define(["jquery"],a):"object"===typeof exports?module.exports=a(require("jquery")):a(l||d)})(function(a){var l=function(b,e,f){var c={invalid:[],getCaret:function(){try{var a,r=0,g=b.get(0),e=document.selection,f=g.selectionStart;if(e&&-1===navigator.appVersion.indexOf("MSIE 10"))a=e.createRange(),a.moveStart("character",-c.val().length),r=a.text.length;else if(f||"0"===f)r=f;return r}catch(C){}},setCaret:function(a){try{if(b.is(":focus")){var c,
g=b.get(0);g.setSelectionRange?g.setSelectionRange(a,a):(c=g.createTextRange(),c.collapse(!0),c.moveEnd("character",a),c.moveStart("character",a),c.select())}}catch(B){}},events:function(){b.on("keydown.mask",function(a){b.data("mask-keycode",a.keyCode||a.which);b.data("mask-previus-value",b.val());b.data("mask-previus-caret-pos",c.getCaret());c.maskDigitPosMapOld=c.maskDigitPosMap}).on(a.jMaskGlobals.useInput?"input.mask":"keyup.mask",c.behaviour).on("paste.mask drop.mask",function(){setTimeout(function(){b.keydown().keyup()},
100)}).on("change.mask",function(){b.data("changed",!0)}).on("blur.mask",function(){d===c.val()||b.data("changed")||b.trigger("change");b.data("changed",!1)}).on("blur.mask",function(){d=c.val()}).on("focus.mask",function(b){!0===f.selectOnFocus&&a(b.target).select()}).on("focusout.mask",function(){f.clearIfNotMatch&&!h.test(c.val())&&c.val("")})},getRegexMask:function(){for(var a=[],b,c,f,n,d=0;d<e.length;d++)(b=m.translation[e.charAt(d)])?(c=b.pattern.toString().replace(/.{1}$|^.{1}/g,""),f=b.optional,
(b=b.recursive)?(a.push(e.charAt(d)),n={digit:e.charAt(d),pattern:c}):a.push(f||b?c+"?":c)):a.push(e.charAt(d).replace(/[-\/\\^$*+?.()|[\]{}]/g,"\\$&"));a=a.join("");n&&(a=a.replace(new RegExp("("+n.digit+"(.*"+n.digit+")?)"),"($1)?").replace(new RegExp(n.digit,"g"),n.pattern));return new RegExp(a)},destroyEvents:function(){b.off("input keydown keyup paste drop blur focusout ".split(" ").join(".mask "))},val:function(a){var c=b.is("input")?"val":"text";if(0<arguments.length){if(b[c]()!==a)b[c](a);
c=b}else c=b[c]();return c},calculateCaretPosition:function(){var a=b.data("mask-previus-value")||"",e=c.getMasked(),g=c.getCaret();if(a!==e){var f=b.data("mask-previus-caret-pos")||0,e=e.length,d=a.length,m=a=0,h=0,l=0,k;for(k=g;k<e&&c.maskDigitPosMap[k];k++)m++;for(k=g-1;0<=k&&c.maskDigitPosMap[k];k--)a++;for(k=g-1;0<=k;k--)c.maskDigitPosMap[k]&&h++;for(k=f-1;0<=k;k--)c.maskDigitPosMapOld[k]&&l++;g>d?g=10*e:f>=g&&f!==d?c.maskDigitPosMapOld[g]||(f=g,g=g-(l-h)-a,c.maskDigitPosMap[g]&&(g=f)):g>f&&
(g=g+(h-l)+m)}return g},behaviour:function(f){f=f||window.event;c.invalid=[];var e=b.data("mask-keycode");if(-1===a.inArray(e,m.byPassKeys)){var e=c.getMasked(),g=c.getCaret();setTimeout(function(){c.setCaret(c.calculateCaretPosition())},a.jMaskGlobals.keyStrokeCompensation);c.val(e);c.setCaret(g);return c.callbacks(f)}},getMasked:function(a,b){var g=[],d=void 0===b?c.val():b+"",n=0,h=e.length,q=0,l=d.length,k=1,r="push",p=-1,t=0,y=[],v,z;f.reverse?(r="unshift",k=-1,v=0,n=h-1,q=l-1,z=function(){return-1<
n&&-1<q}):(v=h-1,z=function(){return n<h&&q<l});for(var A;z();){var x=e.charAt(n),w=d.charAt(q),u=m.translation[x];if(u)w.match(u.pattern)?(g[r](w),u.recursive&&(-1===p?p=n:n===v&&n!==p&&(n=p-k),v===p&&(n-=k)),n+=k):w===A?(t--,A=void 0):u.optional?(n+=k,q-=k):u.fallback?(g[r](u.fallback),n+=k,q-=k):c.invalid.push({p:q,v:w,e:u.pattern}),q+=k;else{if(!a)g[r](x);w===x?(y.push(q),q+=k):(A=x,y.push(q+t),t++);n+=k}}d=e.charAt(v);h!==l+1||m.translation[d]||g.push(d);g=g.join("");c.mapMaskdigitPositions(g,
y,l);return g},mapMaskdigitPositions:function(a,b,e){a=f.reverse?a.length-e:0;c.maskDigitPosMap={};for(e=0;e<b.length;e++)c.maskDigitPosMap[b[e]+a]=1},callbacks:function(a){var h=c.val(),g=h!==d,m=[h,a,b,f],q=function(a,b,c){"function"===typeof f[a]&&b&&f[a].apply(this,c)};q("onChange",!0===g,m);q("onKeyPress",!0===g,m);q("onComplete",h.length===e.length,m);q("onInvalid",0<c.invalid.length,[h,a,b,c.invalid,f])}};b=a(b);var m=this,d=c.val(),h;e="function"===typeof e?e(c.val(),void 0,b,f):e;m.mask=
e;m.options=f;m.remove=function(){var a=c.getCaret();m.options.placeholder&&b.removeAttr("placeholder");b.data("mask-maxlength")&&b.removeAttr("maxlength");c.destroyEvents();c.val(m.getCleanVal());c.setCaret(a);return b};m.getCleanVal=function(){return c.getMasked(!0)};m.getMaskedVal=function(a){return c.getMasked(!1,a)};m.init=function(d){d=d||!1;f=f||{};m.clearIfNotMatch=a.jMaskGlobals.clearIfNotMatch;m.byPassKeys=a.jMaskGlobals.byPassKeys;m.translation=a.extend({},a.jMaskGlobals.translation,f.translation);
m=a.extend(!0,{},m,f);h=c.getRegexMask();if(d)c.events(),c.val(c.getMasked());else{f.placeholder&&b.attr("placeholder",f.placeholder);b.data("mask")&&b.attr("autocomplete","off");d=0;for(var l=!0;d<e.length;d++){var g=m.translation[e.charAt(d)];if(g&&g.recursive){l=!1;break}}l&&b.attr("maxlength",e.length).data("mask-maxlength",!0);c.destroyEvents();c.events();d=c.getCaret();c.val(c.getMasked());c.setCaret(d)}};m.init(!b.is("input"))};a.maskWatchers={};var d=function(){var b=a(this),e={},f=b.attr("data-mask");
b.attr("data-mask-reverse")&&(e.reverse=!0);b.attr("data-mask-clearifnotmatch")&&(e.clearIfNotMatch=!0);"true"===b.attr("data-mask-selectonfocus")&&(e.selectOnFocus=!0);if(p(b,f,e))return b.data("mask",new l(this,f,e))},p=function(b,e,f){f=f||{};var c=a(b).data("mask"),d=JSON.stringify;b=a(b).val()||a(b).text();try{return"function"===typeof e&&(e=e(b)),"object"!==typeof c||d(c.options)!==d(f)||c.mask!==e}catch(t){}},h=function(a){var b=document.createElement("div"),d;a="on"+a;d=a in b;d||(b.setAttribute(a,
"return;"),d="function"===typeof b[a]);return d};a.fn.mask=function(b,d){d=d||{};var e=this.selector,c=a.jMaskGlobals,h=c.watchInterval,c=d.watchInputs||c.watchInputs,t=function(){if(p(this,b,d))return a(this).data("mask",new l(this,b,d))};a(this).each(t);e&&""!==e&&c&&(clearInterval(a.maskWatchers[e]),a.maskWatchers[e]=setInterval(function(){a(document).find(e).each(t)},h));return this};a.fn.masked=function(a){return this.data("mask").getMaskedVal(a)};a.fn.unmask=function(){clearInterval(a.maskWatchers[this.selector]);
delete a.maskWatchers[this.selector];return this.each(function(){var b=a(this).data("mask");b&&b.remove().removeData("mask")})};a.fn.cleanVal=function(){return this.data("mask").getCleanVal()};a.applyDataMask=function(b){b=b||a.jMaskGlobals.maskElements;(b instanceof a?b:a(b)).filter(a.jMaskGlobals.dataMaskAttr).each(d)};h={maskElements:"input,td,span,div",dataMaskAttr:"*[data-mask]",dataMask:!0,watchInterval:300,watchInputs:!0,keyStrokeCompensation:10,useInput:!/Chrome\/[2-4][0-9]|SamsungBrowser/.test(window.navigator.userAgent)&&
h("input"),watchDataMask:!1,byPassKeys:[9,16,17,18,36,37,38,39,40,91],translation:{0:{pattern:/\d/},9:{pattern:/\d/,optional:!0},"#":{pattern:/\d/,recursive:!0},A:{pattern:/[a-zA-Z0-9]/},S:{pattern:/[a-zA-Z]/}}};a.jMaskGlobals=a.jMaskGlobals||{};h=a.jMaskGlobals=a.extend(!0,{},h,a.jMaskGlobals);h.dataMask&&a.applyDataMask();setInterval(function(){a.jMaskGlobals.watchDataMask&&a.applyDataMask()},h.watchInterval)},window.jQuery,window.Zepto);

var ModelList = {
		
		/*Types*/
		'typeNames' : [
			'Low Profile (LOP)',
			'Medium Profile (MP)',
			'Center Mount (CM/LVCM)',
			'Large Unit Cooler (LUC)'
		],'typeKeys' : {
			'Low Profile (LOP)'		 :0,
			'Medium Profile (MP)'	 :1,
			'Center Mount (CM/LVCM)' :2,
			'Large Unit Cooler (LUC)':3
		},

		/*Number of Fans per Type*/
		'numberOfFans':[
			[1,2,3,4,5,6],
			[1,2,3,4,5],
			[1,2,3,4],
			[2,3,4]
		],

		/*models according to type and number of fans*/
		'fansPerTypeKey':[
			{
				1:['LET035','LET040','LET047','LLE041'],
				2:['LET065','LET075','LLE068','LLE080','LET090'],
				3:['LET120','LLE102','LET140'],
				4:['LET160','LET180','LLE136'],
				5:['LET200','LLE170'],
				6:['LET240','LET280','LLE204','LLE235']
			},{
				1:['BME101','BME140','BML100'],
				2:['BME190','BME260','BML165','BML220'],
				3:['BME310','BME390','BML250','BML330'],
				4:['BME430','BME520','BML370','BML440'],
				5:['BME620','BML530']
			},{
				1:['SME040'],
				2:['SME054','SME065'],
				3:['SME090','SME130'],
				4:['SME174']
			},{
				2:['BHE450','BHE550','BHL400','BHL480'],
				3:['BHE640','BHE740','BHE1020','BHE1200','BHE1390','BHE1650','BHL560','BHL650','BHL890','BHL1050','BHL1220','BHL1440'],
				4:['BHE810','BHE950','BHE2120','BHL710','BHL840','BHL1860']
			}
		],'fansPerTypeName':{
			'Low Profile (LOP)':{
				1:['LET035','LET040','LET047','LLE041'],
				2:['LET065','LET075','LLE068','LLE080','LET090'],
				3:['LET120','LLE102','LET140'],
				4:['LET160','LET180','LLE136'],
				5:['LET200','LLE170'],
				6:['LET240','LET280','LLE204','LLE235']
			},'Medium Profile (MP)':{
				1:['BME101','BME140','BML100'],
				2:['BME190','BME260','BML165','BML220'],
				3:['BME310','BME390','BML250','BML330'],
				4:['BME430','BME520','BML370','BML440'],
				5:['BME620','BML530']
			},'Center Mount (CM/LVCM)':{
				1:['SME040'],
				2:['SME054','SME065'],
				3:['SME090','SME130'],
				4:['SME174']
			},'Large Unit Cooler (LUC)':{
				2:['BHE450','BHE550','BHL400','BHL480'],
				3:['BHE640','BHE740','BHE1020','BHE1200','BHE1390','BHE1650','BHL560','BHL650','BHL890','BHL1050','BHL1220','BHL1440'],
				4:['BHE810','BHE950','BHE2120','BHL710','BHL840','BHL1860']
			}
		},

		/*WATT & THP per model*/
		'model':{
			'LET035'	: {'WATT':900, 	'THP':900},
			'LET040'	: {'WATT':900, 	'THP':900},
			'LET047'	: {'WATT':900, 	'THP':900},
			'LET065'	: {'WATT':900, 	'THP':1800},
			'LET075'	: {'WATT':900, 	'THP':1800},
			'LET090'	: {'WATT':900, 	'THP':1800},
			'LET120'	: {'WATT':900, 	'THP':2700},
			'LET140'	: {'WATT':900, 	'THP':2700},
			'LET160'	: {'WATT':900, 	'THP':3600},
			'LET180'	: {'WATT':900, 	'THP':3600},
			'LET200'	: {'WATT':900, 	'THP':4500},
			'LET240'	: {'WATT':900, 	'THP':5400},
			'LET280'	: {'WATT':900, 	'THP':5400},
			'LLE041'	: {'WATT':900, 	'THP':900},
			'LLE068'	: {'WATT':900, 	'THP':1800},
			'LLE080'	: {'WATT':900, 	'THP':1800},
			'LLE102'	: {'WATT':900, 	'THP':2700},
			'LLE136'	: {'WATT':900, 	'THP':3600},
			'LLE170'	: {'WATT':900, 	'THP':4500},
			'LLE204'	: {'WATT':900, 	'THP':5400},
			'LLE235'	: {'WATT':900, 	'THP':5400},
			'BME101'	: {'WATT':2730,	'THP':2730},
			'BME140'	: {'WATT':2730,	'THP':2730},
			'BME190'	: {'WATT':2675,	'THP':5350},
			'BME260'	: {'WATT':2675,	'THP':5350},
			'BME310'	: {'WATT':2583,	'THP':7750},
			'BME390'	: {'WATT':2583,	'THP':7750},
			'BME430'	: {'WATT':2550,	'THP':10200},
			'BME520'	: {'WATT':2550,	'THP':10200},
			'BME620'	: {'WATT':2320,	'THP':11600},
			'BML100'	: {'WATT':2730,	'THP':2730},
			'BML165'	: {'WATT':2675,	'THP':5350},
			'BML220'	: {'WATT':2675,	'THP':5350},
			'BML250'	: {'WATT':2583,	'THP':7750},
			'BML330'	: {'WATT':2583,	'THP':7750},
			'BML370'	: {'WATT':2550,	'THP':10200},
			'BML440'	: {'WATT':2550,	'THP':10200},
			'BML530'	: {'WATT':2320,	'THP':11600},
			'SME040'	: {'WATT':1200,	'THP':1200},
			'SME054'	: {'WATT':1000,	'THP':2000},
			'SME065'	: {'WATT':1000,	'THP':2000},
			'SME090'	: {'WATT':800, 	'THP':2400},
			'SME130'	: {'WATT':1200,	'THP':3600},
			'SME174'	: {'WATT':1200,	'THP':4800},
			'BHE450'	: {'WATT':4950,	'THP':9900},
			'BHE550'	: {'WATT':4950,	'THP':9900},
			'BHE640'	: {'WATT':4300,	'THP':12900},
			'BHE740'	: {'WATT':4300,	'THP':12900},
			'BHE810'	: {'WATT':4263,	'THP':17050},
			'BHE950'	: {'WATT':4263,	'THP':17050},
			'BHE1020'	: {'WATT':7133,	'THP':21400},
			'BHE1200'	: {'WATT':7133,	'THP':21400},
			'BHE1390'	: {'WATT':7133,	'THP':21400},
			'BHE1650'	: {'WATT':11200,'THP':33600},
			'BHE2120'	: {'WATT':12463,'THP':49850},
			'BHL400'	: {'WATT':4950,	'THP':9900},
			'BHL480'	: {'WATT':4950,	'THP':9900},
			'BHL560'	: {'WATT':4300,	'THP':12900},
			'BHL650'	: {'WATT':4300,	'THP':12900},
			'BHL710'	: {'WATT':4263,	'THP':17050},
			'BHL840'	: {'WATT':4263,	'THP':17050},
			'BHL890'	: {'WATT':7133,	'THP':21400},
			'BHL1050'	: {'WATT':7133,	'THP':21400},
			'BHL1220'	: {'WATT':7133,	'THP':21400},
			'BHL1440'	: {'WATT':11200,'THP':33600},
			'BHL1860'	: {'WATT':12463,'THP':49850}
		},

		/*Price Adder per type*/
		'intelliGenPriceAdder':[636.74, 903.90, 636.74, 1580.13],
		'mechCompPriceAdder'  :[526.93, 699.03, 567.06, 817.84],

		/*Energy price per state*/
		'energyPrice':{
			'AL':0.0989,
			'AK':0.1952,
			'AZ':0.1071,
			'AR':0.0818,
			'CA':0.1614,
			'CO':0.0994,
			'CT':0.1762,
			'DE':0.1099,
			'DC':0.1181,
			'FL':0.1065,
			'GA':0.0975,
			'HI':0.2607,
			'ID':0.0830,
			'IL':0.0933,
			'IN':0.0961,
			'IA':0.0892,
			'KS':0.1058,
			'KY':0.0844,
			'LA':0.0775,
			'ME':0.1294,
			'MD':0.1200,
			'MA':0.1614,
			'MI':0.1139,
			'MN':0.1053,
			'MS':0.0919,
			'MO':0.0983,
			'MT':0.0902,
			'NE':0.0916,
			'NV':0.0876,
			'NH':0.1616,
			'NJ':0.1338,
			'NM':0.0964,
			'NY':0.1478,
			'NC':0.0915,
			'ND':0.0926,
			'OH':0.0971,
			'OK':0.0812,
			'OR':0.0898,
			'PA':0.1016,
			'RI':0.1644,
			'SC':0.0983,
			'SD':0.0998,
			'TN':0.0954,
			'TX':0.0855,
			'UT':0.0866,
			'VT':0.1457,
			'VA':0.0928,
			'WA':0.0794,
			'WV':0.0900,
			'WI':0.1105,
			'WY':0.0829
		}
	};

	var ADTadm = 182.5;

	/*Energy price*/
	var EP = 0;

	/*All instances will be saved on this Array*/
	var allSystems = [];

	/*Current Number of created System instances for html id creation*/
	var currentNumber = 0;

	/*To add instances of System to array*/
	function addSystem(){
		
		/*Add System instance to array*/
		allSystems[allSystems.length] = new System(currentNumber);

		/*Increase Current Number of instances*/
		currentNumber++;

		/*Organize html Titles independent of current number*/
		organizeTitles();
	}
	function organizeTitles(){
		/*To organize titles html*/
		/*The first html block don't have Title*/
		/*All others html blocks have "System(n+1)" title */

		for (var i = 0; i < allSystems.length; i++) {

			/*Tell each instance to individually change your title*/
			allSystems[i].changeTitle("System " + (i+1));
		}
	}
	function cleanSystem(){
		/*Check for disabled instances*/
		/*then delete and remove it from the array*/

		for (var i = allSystems.length - 1; i >= 0; i--) {
			if(allSystems[i].enabled === false){
				delete allSystems[i];
				allSystems.splice(i, 1);
			}
		}

		/*Then organize the titles*/
		organizeTitles();
	}

	/*Factory function for System class*/
	function System(n){

		/*Save the instance reference*/
		var it = this;

		
		/********************************************/
		/************** Method Session **************/

		this.autoDestruct = function (){
			/*Its method disable and delete the html that refers this instance*/

			/*Disable*/
			it.enabled = false;
			
			/*UX - fade out when delete*/
			it.bx.animate({
				'overflow':'hidden',
				'height'  :'0px',
				'opacity' :'0'
			},300,function(){
				/*Delete html*/
				$(this).remove();
			});

			/*Prompts to delete disabled Instances*/
			cleanSystem();
		}
		this.appendTheHtml = function(){
			/*This method prepares and append a box for this instance*/

			/*Get the saved sample int the textarea*/
			var html = $("#SystemExample").val();

			/*Exchanges all "${number}" for the current number*/
			html = html.split("${number}").join(it.number);

			/*Append the html to step 1*/
			$('#bxContainer').append(html); 

			/*Tooltip to info icons*/
			$('[data-toggle="tooltip"]').tooltip();
		}
		this.applyMask = function(){
			/*This method applies the jquery mask to the user-editable price fields*/

			it.intelliGenPriceAdder.mask("#0.00", {reverse: true});
			it.mechCompPriceAdder.mask("#0.00", {reverse: true});
		}
		this.changeTitle = function(string){
			/*Change the html title of this instance if there is a value in the string*/
			
			it.title = string;
			if(string){
				it.headerTitle.text(string);
				it.headerTitle.show();
			} else {
				it.headerTitle.hide();
			}
		}
		this.priceDiferenceAtt = function(){
			/*IntelliGen Upcharge*/
			/*Calculate the price difference*/

			var intelligen = parseFloat(it.intelliGenPriceAdder.val());
			var mechComp = parseFloat(it.mechCompPriceAdder.val());
			var diference = intelligen - mechComp;
			
			/*Two decimal places only*/
			diference = diference.toFixed(2);

			/*Change upcharde input value*/
			it.priceDiference.val(diference);
		}
		this.organizeFansPerEvaporatorSelect = function(){
			/*This method changes the fans selector according to the type*/

			/*Get evaporator type*/
			var type = it.evaporatorType.val();
			
			/*Get the array with the number of fans list according to the type*/
			var numberOfFans = ModelList['numberOfFans'][type];

			/*Creates an html option to put in the number of Fans selector*/
			var html = '';
			for (var i = 0; i < numberOfFans.length; i++) {
				html += "<option value='" + numberOfFans[i] + "'>" + numberOfFans[i] + "</option>";
			}

			/*Append the html options*/
			it.fansPerEvaporator.html(html);
		}
		this.organizeModelsSelect = function(){
			/*This method change model select options according the type*/

			/*Get evaporator type*/
			var type = it.evaporatorType.val();

			/*This variable stores all models according to the type*/
			var fansPerTypeKey = ModelList['fansPerTypeKey'][type];

			/*Get number of fans*/
			var fansPerEvaporator = it.fansPerEvaporator.val();
			
			/*This variable stores all models according to the type and number of fans*/
			var models = fansPerTypeKey[fansPerEvaporator];

			/*Creates an html of options for the template selector*/
			var html = '';
			for (var i = 0; i < models.length; i++) {
				html += "<option value='" + models[i] + "'>" + models[i] + "</option>";
			}

			/*Append html*/
			it.evaporatorModel.html(html);
		}
		this.changeBothPriceAdder = function(){
			/*This method changes the value of the price adder fields according to the type*/

			var type = it.evaporatorType.val();
			it.intelliGenPriceAdder.val(ModelList['intelliGenPriceAdder'][type]);
			it.mechCompPriceAdder.val(ModelList['mechCompPriceAdder'][type]);
			
			/*Recalculate the difference between them*/
			it.priceDiferenceAtt();
		}
		this.onChangeModel = function(){
			/*This method changes the value of WATT when the model is changed*/

			var model 	= it.evaporatorModel.val();
			it.G = model;
			it.THP 	= ModelList['model'][model]['THP'];
			it.WATT 	= ModelList['model'][model]['WATT'];
		}
		this.calcule = function(){
			/*This method makes the individual calculation of Defrost Reduction*/
			/*And saves in a property for the total calculation*/

			it.A = ModelList['typeNames'][parseFloat(it.evaporatorType.val())];
			it.B = parseFloat(it.fansPerEvaporator.val());
			it.C = parseFloat(it.numberOfEvaporators.val());
			it.D = parseFloat(it.coldRooms.val());
			it.E = parseFloat(it.defrostDuration.val());
			it.F = parseFloat(it.defrostsDay.val());
			it.IPA = parseFloat(it.intelliGenPriceAdder.val());
			it.MPA = parseFloat(it.mechCompPriceAdder.val());
			it.ADT = (365 * it.E * it.F) / 60;
			it.PA = it.C * it.D * (it.IPA - it.MPA);
			it.DF = it.B * it.C * it.D * it.WATT * EP * (it.ADT - ADTadm) / 1000;
			it.HP = it.THP/1000;
			it.AHCadm = ADTadm * it.HP * it.C * it.D;
			it.AHC = it.ADT * it.HP * it.C * it.D;
			it.ADCadm = it.AHCadm * EP;
			it.ADC = it.AHC * EP;
			it.TPA = it.C * it.D * it.IPA;
			it.TPAM = it.C * it.D * it.MPA;
			it.ROI = (it.TPA - it.TPAM)/ it.DF;
		}
		this.report = function(){
			return {
				'A':it.A,
				'B':it.B,
				'C':it.C,
				'D':it.D,
				'E':it.E,
				'F':it.F,
				'G':it.G,
				'HP':it.HP,
				'IPA':it.IPA,
				'MPA':it.MPA,
				'THP':it.THP,
				'WATT':it.WATT,
				'ADT':it.ADT,
				'AHCadm':it.AHCadm,
				'AHC':it.AHC,
				'TPA':it.TPA,
				'TPAM':it.TPAM,
				'PA':it.PA,
				'DF':it.DF,
				'ROI':it.ROI
			};
		}


		/************** Method Session **************/
		/***************** the end ******************/



		/*Constructor*/
		this.enabled = true;	/*Enable Instance*/
		this.title = '';		
		this.number = n;		/*Save your reference number for id purposes*/
		this.appendTheHtml();	/*Create the html for instance*/

		this.A = 0;
		this.B = 0;
		this.C = 0;
		this.D = 0;
		this.E = 0;
		this.F = 0;
		this.G = 'undefined model';
		this.HP = 0;
		this.IPA = 0;
		this.MPA = 0;
		this.THP = 0;
		this.WATT = 0;
		this.ADT = 0;
		this.AHCadm = 0;
		this.AHC = 0;
		this.TPA = 0;
		this.TPAM = 0;
		this.PA = 0;
		this.DF = 0;
		this.ROI = 0;
		
		/*Save all nodes for DOM manipulation*/
		this.bx						= $("#bx" + n);
		this.headerTitle			= $("#headerTitle" + n);
		this.evaporatorType			= $("#evaporatorType" + n);
		this.fansPerEvaporator		= $("#fansPerEvaporator" + n);
		this.numberOfEvaporators	= $("#numberOfEvaporators" + n);
		this.coldRooms				= $("#coldRooms" + n);
		this.defrostDuration		= $("#defrostDuration" + n);
		this.defrostsDay			= $("#defrostsDay" + n);
		this.evaporatorModel		= $("#evaporatorModel" + n);
		this.intelliGenPriceAdder 	= $("#intelliGenPriceAdder" + n);
		this.mechCompPriceAdder 	= $("#mechCompPriceAdder" + n);
		this.priceDiference 		= $("#priceDiference" + n);
		this.advancedOptions		= $("#advancedOptions" + n);
		this.buttonAdvancedOptions	= $("#buttonAdvancedOptions" + n);
		this.deleteSystem			= $("#deleteSystem" + n);
		this.applyMask();		/*Apply Money Mask to inputs*/
		this.priceDiferenceAtt();
		this.organizeFansPerEvaporatorSelect();
		this.organizeModelsSelect();
		this.changeBothPriceAdder();
		this.onChangeModel();

		/*Start event Listeners*/

		/*Delete this instance*/
		this.deleteSystem.on('click', function(){
			it.autoDestruct();
		});

		/*toggle the visibility of advanced options*/
		this.buttonAdvancedOptions.on('click', function(){
			it.advancedOptions.slideToggle(100);
		});


		/*When choosing another type of evaporator*/
		this.evaporatorType.on('input change', function(e){
			it.organizeFansPerEvaporatorSelect();
			it.organizeModelsSelect();
			it.changeBothPriceAdder();
			it.onChangeModel();
		});

		/*When you choose the number of fans per evaporator*/
		this.fansPerEvaporator.on('input change', function(e){
			it.organizeModelsSelect();
			it.onChangeModel();
		});

		/*When you choose the evaporator model*/
		this.evaporatorModel.on('input change', function(e){
			it.onChangeModel();
		});

		/*When the IntelliGen price adder is changed*/
		this.intelliGenPriceAdder.on('input change keypress',function(e){
			it.priceDiferenceAtt();
		});
		/*When the IntelliGen price adder focus out*/
		this.intelliGenPriceAdder.on('blur', function(e){
			var value = $(this).val();
			if(value == '' || !+value){
				/*When the price adder value is empty*/
				var type = it.evaporatorType.val();

				/*This takes the default value*/
				$(this).val(ModelList['intelliGenPriceAdder'][type].toFixed(2));				

				it.priceDiferenceAtt();
			}
		});

		/*When the Mech. Comp. price adder is changed*/
		this.mechCompPriceAdder.on('input change keypress',function(e){
			it.priceDiferenceAtt();
		});
		/*When the Mech. Comp. price adder focus out*/
		this.mechCompPriceAdder.on('blur', function(e){
			var value = $(this).val();
			if(value == '' || !+value){
				/*When the price adder value is empty*/
				var type = it.evaporatorType.val();

				/*This takes the default value*/
				$(this).val(ModelList['mechCompPriceAdder'][type].toFixed(2));				

				it.priceDiferenceAtt();
			}
		});
	}




	/*Helper functions*/

	function in_period(value, min, max){
		/*Check if the value is within the limits*/

		if(!value || typeof value !== 'number'){
			return false;
		}
		if(value > min && value < max){
			return true;
		}
		return false;
	}
	function disableSelects(){
		/*Disables selectors when the box not in focus*/
		$('.step select').removeAttr('disabled');
		$('.step.disabled select').attr('disabled',true);
	}
	function renderStep1(){
 		/*Changes to block 1*/
		$('#step1').removeClass('disabled');
		$('#step2').addClass('disabled');
		$('#step3').addClass('disabled');
		disableSelects();
		$('#littleDot1').addClass('active');
		$('#littleDot2').removeClass('active');
		$('#littleDot3').removeClass('active');
		$("#highROIT").removeClass('visible');
		if ($('body').width()<992){
			$('html, body').animate({
				scrollTop: $("#step1").offset().top
			}, 500, 'linear');
		}
	}
	function renderStep2(){
		/*Changes to block 2*/
		$('#step1').addClass('disabled');
		$('#step2').removeClass('disabled');
		$('#step3').addClass('disabled');
		disableSelects();
		$('#littleDot1').removeClass('active');
		$('#littleDot2').addClass('active');
		$('#littleDot3').removeClass('active');
		$("#highROIT").removeClass('visible');
		if ($('body').width()<992){
			$('html, body').animate({
				scrollTop: $("#step2").offset().top
			}, 500, 'linear');
		}
	}
	function renderStep3(){
		/*Changes to block 3*/
		$('#step1').addClass('disabled');
		$('#step2').addClass('disabled');
		$('#step3').removeClass('disabled');
		disableSelects();
		$('#littleDot1').removeClass('active');
		$('#littleDot2').removeClass('active');
		$('#littleDot3').addClass('active');
		$("#highROIT").removeClass('visible');
		if ($('body').width()<992){
			$('html, body').animate({
				scrollTop: $("#step3").offset().top
			}, 500, 'linear');
		}
	}
	/*End | helper fuctions*/


	/*object to json report*/
	var report_obj = {'System':[]};
	
	/*Calculation*/
	var DFT = 0;
	var PAT = 0;
	var ROIT = 0;
	function calcule(){
		report_obj['System'] = [];
		DFT = 0;
		PAT = 0;
		for (var i = 0; i < allSystems.length; i++) {
			var system = allSystems[i];
			system.calcule();
			PAT += system.PA;
			DFT += system.DF;

			/*Add System internal report to report object*/
			report_obj['System'].push(system.report());
		}
		ROIT = PAT/DFT;

		/*Update report object with fixed values*/
		report_obj['EP'] = EP;
		report_obj['DFT'] = DFT;
		report_obj['PAT'] = PAT;
		report_obj['ROIT'] = ROIT;
		console.log(report_obj);

		
		DFT = DFT.toFixed(2);
		ROIT = ROIT.toFixed(2);
	}
	/*End | Calculation*/


	$(document).ready(function(){
		disableSelects();
		
		/*Add one System*/
		addSystem();
		$("#buttonAddOption").on('click', function(){
			/*Add another System when click the button*/
			addSystem();
		});


		/*Setps controller*/

		/*First Step*/
		$("#backButton, #littleDot1").on('click', renderStep1);

		/*Second Step*/
		$("#nextButton, #littleDot2, #backCalcButton").on('click', renderStep2);


		/*Step 2 Listeners & energy price*/
		var avarageCost = $("#avarageCost");
		var stateSelector = $("#stateSelector");
		
		/*Apply jquery mask to Avarage Cost input*/
		avarageCost.mask("0r0000", {
			translation: {
				'r': {
					pattern: /[.]/,
					fallback: '.'
				},
				placeholder: "_.___"
			}
		})

		/*Before Select of states is ready initialize the Energy Price varible*/
		EP = ModelList['energyPrice'][stateSelector.val()];

		/*Change the Avarage Cost input value to Energy Price*/
		avarageCost.val(EP);

		/*Select state change Listener*/	
		stateSelector.on('input change', function(){
			
			/*On change State change Energy Price*/
			EP = ModelList['energyPrice'][$(this).val()];
			
			/*Change the Avarage Cost input value to Energy Price*/
			avarageCost.val(EP);
		});

		/*Avarage Cost blur listener*/
		avarageCost.on('blur',function(){

			/*Get Avarage Cost value*/
			var val = $(this).val();

			/*Validate the value*/
			if(+val == NaN || parseFloat(val) == NaN || parseFloat(val) <= 0 || parseFloat(val) == undefined || parseFloat(val) == null || parseFloat(val) == Infinity || parseFloat(val) == false){
				/*When is not a valid number*/

				/*Change Energy price according to the select State value*/
				EP = ModelList['energyPrice'][stateSelector.val()];

				/*Change the Avarage Cost input value to Energy Price*/
				$(this).val(EP);
				return;
			}

			/*Change Energy Price to Avarage Cost value*/
			EP = +val || parseFloat(val);
		});

		/*When press Calculate button*/
		$('#calcButton, #littleDot3').on('click', function(){
			
			/*Calcule ROIT*/
			calcule(); 

			/*Show final Step*/
			renderStep3();

			/*Change the money text of Defrost reduction per year*/
			$("#defrostReduction").text('$' + DFT);
			
			/*Change the text of ROI per year*/
			$("#returnInvestment").text(ROIT);


			if(ROIT > 2.5){
				/*When ROIT is greater than 1.5 show the message*/
				$("#highROIT").addClass('visible');
			}
		});
	});